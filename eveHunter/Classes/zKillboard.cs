﻿using System;
using System.Net;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace eveHunter.Classes
{
    [DataContract]
    class zKillboard
    {
        public zKillboard() { }

        [DataMember(Name = "hash")]
        String Hash { get; set; }
        [DataMember(Name = "totalValue")]
        double TotalValue { get; set; }
        [DataMember(Name = "points")]
        long Points { get; set; }
        [DataMember(Name = "killID")]
        long KillID { get; set; }
        [DataMember(Name = "solarSystemID")]
        long SolarSystemID { get; set; }
        [DataMember(Name = "killTime")]
        DateTime KillTime { get; set; }
        [DataMember(Name = "moonID")]
        long MoonID { get; set; }
        [DataMember(Name = "victim")]
        Character Victim { get; set; }
        [DataMember(Name = "attackers")]
        Character[] Attackers { get; set; }
        [DataMember(Name = "items")]
        Item[] Items { get; set; }

        #region Functions


        public static zKillboard GetKillmail(string kmURL = "")
        {
            try
            {
                HttpWebRequest request = WebRequest.Create(kmURL) as HttpWebRequest;
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                        throw new Exception(String.Format(
                        "Server error (HTTP {0}: {1}).",
                        response.StatusCode,
                        response.StatusDescription));
                    DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(zKillboard));
                    object objResponse = jsonSerializer.ReadObject(response.GetResponseStream());
                    zKillboard jsonZKB = objResponse as zKillboard;
                    return jsonZKB;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }
        #endregion

        #region Classes
        [DataContract]
        class Character
        {

            [DataMember(Name = "characterID")]
            long CharacterID { get; set; }
            [DataMember(Name = "characterName")]
            String CharacterName { get; set; }
            [DataMember(Name = "corporationID")]
            long CorporationID { get; set; }
            [DataMember(Name = "corporationName")]
            String CorporationName { get; set; }
            [DataMember(Name = "allianceID")]
            long AllianceID { get; set; }
            [DataMember(Name = "allianceName")]
            String AllianceName { get; set; }
            [DataMember(Name = "factionID")]
            long FactionID { get; set; }
            [DataMember(Name = "factionName")]
            String FactionName { get; set; }
            [DataMember(Name = "securityStatus")]
            int SecurityStatus { get; set; }
            [DataMember(Name = "damageDone")]
            long DamageDone { get; set; }
            [DataMember(Name = "damageTaken")]
            long DamageTaken { get; set; }
            [DataMember(Name = "finalBlow")]
            bool FinalBlow { get; set; }
            [DataMember(Name = "weaponTypeID")]
            long WeaponTypeID { get; set; }
            [DataMember(Name = "shipTypeID")]
            long ShipTypeID { get; set; }
        }
        [DataContract]
        class Item
        {
            [DataMember(Name = "typeID")]
            long TypeID { get; set; }
            [DataMember(Name = "flag")]
            long Flag { get; set; }
            [DataMember(Name = "qtyDropped")]
            int QuantityDropped { get; set; }
            [DataMember(Name = "qtyDestroyed")]
            int QuantityDestroyed { get; set; }
            [DataMember(Name = "singleton")]
            bool Singleton { get; set; }
        }
        #endregion
    }
}


/*
 * https://zkillboard.com/api/kills/characterID/92994416/    test url
 * https://msdn.microsoft.com/en-us/library/hh674188.aspx
    try
    {
        HttpWebRequest request = WebRequest.Create(requestUrl) as HttpWebRequest;
        using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
        {
            if (response.StatusCode != HttpStatusCode.OK)
                throw new Exception(String.Format(
                "Server error (HTTP {0}: {1}).",
                response.StatusCode,
                response.StatusDescription));
            DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(Response));
            object objResponse = jsonSerializer.ReadObject(response.GetResponseStream());
            Response jsonResponse
            = objResponse as Response;
            return jsonResponse;
        }
    }
    catch (Exception e)
    {
        Console.WriteLine(e.Message);
        return null;
    }
*/