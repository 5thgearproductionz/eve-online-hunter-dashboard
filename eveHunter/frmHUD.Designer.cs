﻿namespace eveHunter
{
    partial class frmHUD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmHUD));
            this.radZKB = new System.Windows.Forms.RadioButton();
            this.radEKnet = new System.Windows.Forms.RadioButton();
            this.radAPI = new System.Windows.Forms.RadioButton();
            this.gbxFilter = new System.Windows.Forms.GroupBox();
            this.chkFilterWSpace = new System.Windows.Forms.CheckBox();
            this.chkFilterSolo = new System.Windows.Forms.CheckBox();
            this.chkFilterLosses = new System.Windows.Forms.CheckBox();
            this.chkFilterKills = new System.Windows.Forms.CheckBox();
            this.bttnFetch = new System.Windows.Forms.Button();
            this.cboMainKillType = new System.Windows.Forms.ComboBox();
            this.cbo2ndType = new System.Windows.Forms.ComboBox();
            this.txtSearchBox = new System.Windows.Forms.TextBox();
            this.bttnSearch = new System.Windows.Forms.Button();
            this.bttnClearHUD = new System.Windows.Forms.Button();
            this.tlpKillDisplay = new System.Windows.Forms.TableLayoutPanel();
            this.listView1 = new System.Windows.Forms.ListView();
            this.gbxFilter.SuspendLayout();
            this.SuspendLayout();
            // 
            // radZKB
            // 
            this.radZKB.AutoSize = true;
            this.radZKB.Location = new System.Drawing.Point(31, 12);
            this.radZKB.Name = "radZKB";
            this.radZKB.Size = new System.Drawing.Size(70, 17);
            this.radZKB.TabIndex = 0;
            this.radZKB.TabStop = true;
            this.radZKB.Text = "zKillboard";
            this.radZKB.UseVisualStyleBackColor = true;
            this.radZKB.CheckedChanged += new System.EventHandler(this.radZKB_CheckedChanged);
            // 
            // radEKnet
            // 
            this.radEKnet.AutoSize = true;
            this.radEKnet.Location = new System.Drawing.Point(135, 12);
            this.radEKnet.Name = "radEKnet";
            this.radEKnet.Size = new System.Drawing.Size(78, 17);
            this.radEKnet.TabIndex = 1;
            this.radEKnet.TabStop = true;
            this.radEKnet.Text = "Eve-Kill.net";
            this.radEKnet.UseVisualStyleBackColor = true;
            this.radEKnet.CheckedChanged += new System.EventHandler(this.radZKB_CheckedChanged);
            // 
            // radAPI
            // 
            this.radAPI.AutoSize = true;
            this.radAPI.Location = new System.Drawing.Point(247, 12);
            this.radAPI.Name = "radAPI";
            this.radAPI.Size = new System.Drawing.Size(42, 17);
            this.radAPI.TabIndex = 2;
            this.radAPI.TabStop = true;
            this.radAPI.Text = "API";
            this.radAPI.UseVisualStyleBackColor = true;
            this.radAPI.CheckedChanged += new System.EventHandler(this.radZKB_CheckedChanged);
            // 
            // gbxFilter
            // 
            this.gbxFilter.Controls.Add(this.chkFilterWSpace);
            this.gbxFilter.Controls.Add(this.chkFilterSolo);
            this.gbxFilter.Controls.Add(this.chkFilterLosses);
            this.gbxFilter.Controls.Add(this.chkFilterKills);
            this.gbxFilter.Location = new System.Drawing.Point(380, 12);
            this.gbxFilter.Name = "gbxFilter";
            this.gbxFilter.Size = new System.Drawing.Size(370, 87);
            this.gbxFilter.TabIndex = 3;
            this.gbxFilter.TabStop = false;
            this.gbxFilter.Text = "Filter";
            // 
            // chkFilterWSpace
            // 
            this.chkFilterWSpace.AutoSize = true;
            this.chkFilterWSpace.Location = new System.Drawing.Point(136, 19);
            this.chkFilterWSpace.Name = "chkFilterWSpace";
            this.chkFilterWSpace.Size = new System.Drawing.Size(71, 17);
            this.chkFilterWSpace.TabIndex = 3;
            this.chkFilterWSpace.Text = "W-Space";
            this.chkFilterWSpace.UseVisualStyleBackColor = true;
            // 
            // chkFilterSolo
            // 
            this.chkFilterSolo.AutoSize = true;
            this.chkFilterSolo.Location = new System.Drawing.Point(11, 62);
            this.chkFilterSolo.Name = "chkFilterSolo";
            this.chkFilterSolo.Size = new System.Drawing.Size(47, 17);
            this.chkFilterSolo.TabIndex = 2;
            this.chkFilterSolo.Text = "Solo";
            this.chkFilterSolo.UseVisualStyleBackColor = true;
            // 
            // chkFilterLosses
            // 
            this.chkFilterLosses.AutoSize = true;
            this.chkFilterLosses.Location = new System.Drawing.Point(11, 39);
            this.chkFilterLosses.Name = "chkFilterLosses";
            this.chkFilterLosses.Size = new System.Drawing.Size(59, 17);
            this.chkFilterLosses.TabIndex = 1;
            this.chkFilterLosses.Text = "Losses";
            this.chkFilterLosses.UseVisualStyleBackColor = true;
            // 
            // chkFilterKills
            // 
            this.chkFilterKills.AutoSize = true;
            this.chkFilterKills.Location = new System.Drawing.Point(11, 16);
            this.chkFilterKills.Name = "chkFilterKills";
            this.chkFilterKills.Size = new System.Drawing.Size(44, 17);
            this.chkFilterKills.TabIndex = 0;
            this.chkFilterKills.Text = "Kills";
            this.chkFilterKills.UseVisualStyleBackColor = true;
            // 
            // bttnFetch
            // 
            this.bttnFetch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bttnFetch.Location = new System.Drawing.Point(12, 557);
            this.bttnFetch.Name = "bttnFetch";
            this.bttnFetch.Size = new System.Drawing.Size(75, 23);
            this.bttnFetch.TabIndex = 4;
            this.bttnFetch.Text = "Get Killmail";
            this.bttnFetch.UseVisualStyleBackColor = true;
            // 
            // cboMainKillType
            // 
            this.cboMainKillType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboMainKillType.FormattingEnabled = true;
            this.cboMainKillType.Location = new System.Drawing.Point(12, 45);
            this.cboMainKillType.Name = "cboMainKillType";
            this.cboMainKillType.Size = new System.Drawing.Size(144, 21);
            this.cboMainKillType.TabIndex = 5;
            this.cboMainKillType.SelectedValueChanged += new System.EventHandler(this.cboMainKillType_SelectedValueChanged);
            // 
            // cbo2ndType
            // 
            this.cbo2ndType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo2ndType.FormattingEnabled = true;
            this.cbo2ndType.Location = new System.Drawing.Point(12, 70);
            this.cbo2ndType.Name = "cbo2ndType";
            this.cbo2ndType.Size = new System.Drawing.Size(237, 21);
            this.cbo2ndType.Sorted = true;
            this.cbo2ndType.TabIndex = 6;
            // 
            // txtSearchBox
            // 
            this.txtSearchBox.Location = new System.Drawing.Point(12, 105);
            this.txtSearchBox.Name = "txtSearchBox";
            this.txtSearchBox.Size = new System.Drawing.Size(237, 20);
            this.txtSearchBox.TabIndex = 8;
            this.txtSearchBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSearchBox_KeyPress);
            // 
            // bttnSearch
            // 
            this.bttnSearch.Location = new System.Drawing.Point(289, 79);
            this.bttnSearch.Name = "bttnSearch";
            this.bttnSearch.Size = new System.Drawing.Size(75, 46);
            this.bttnSearch.TabIndex = 9;
            this.bttnSearch.Text = "Search";
            this.bttnSearch.UseVisualStyleBackColor = true;
            this.bttnSearch.Click += new System.EventHandler(this.bttnSearch_Click);
            // 
            // bttnClearHUD
            // 
            this.bttnClearHUD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bttnClearHUD.Location = new System.Drawing.Point(138, 557);
            this.bttnClearHUD.Name = "bttnClearHUD";
            this.bttnClearHUD.Size = new System.Drawing.Size(75, 23);
            this.bttnClearHUD.TabIndex = 10;
            this.bttnClearHUD.Text = "Clear All";
            this.bttnClearHUD.UseVisualStyleBackColor = true;
            // 
            // tlpKillDisplay
            // 
            this.tlpKillDisplay.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tlpKillDisplay.BackgroundImage")));
            this.tlpKillDisplay.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tlpKillDisplay.ColumnCount = 3;
            this.tlpKillDisplay.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19F));
            this.tlpKillDisplay.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19F));
            this.tlpKillDisplay.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 62F));
            this.tlpKillDisplay.Location = new System.Drawing.Point(380, 105);
            this.tlpKillDisplay.Name = "tlpKillDisplay";
            this.tlpKillDisplay.RowCount = 4;
            this.tlpKillDisplay.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tlpKillDisplay.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpKillDisplay.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpKillDisplay.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpKillDisplay.Size = new System.Drawing.Size(370, 475);
            this.tlpKillDisplay.TabIndex = 11;
            // 
            // listView1
            // 
            this.listView1.Location = new System.Drawing.Point(12, 146);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(352, 399);
            this.listView1.TabIndex = 12;
            this.listView1.UseCompatibleStateImageBehavior = false;
            // 
            // frmHUD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(762, 596);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.tlpKillDisplay);
            this.Controls.Add(this.bttnClearHUD);
            this.Controls.Add(this.bttnSearch);
            this.Controls.Add(this.txtSearchBox);
            this.Controls.Add(this.cbo2ndType);
            this.Controls.Add(this.cboMainKillType);
            this.Controls.Add(this.bttnFetch);
            this.Controls.Add(this.gbxFilter);
            this.Controls.Add(this.radAPI);
            this.Controls.Add(this.radEKnet);
            this.Controls.Add(this.radZKB);
            this.Name = "frmHUD";
            this.Text = "frmHUD";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmHUD_FormClosing);
            this.Load += new System.EventHandler(this.frmHUD_Load);
            this.gbxFilter.ResumeLayout(false);
            this.gbxFilter.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radZKB;
        private System.Windows.Forms.RadioButton radEKnet;
        private System.Windows.Forms.RadioButton radAPI;
        private System.Windows.Forms.GroupBox gbxFilter;
        private System.Windows.Forms.Button bttnFetch;
        private System.Windows.Forms.CheckBox chkFilterWSpace;
        private System.Windows.Forms.CheckBox chkFilterSolo;
        private System.Windows.Forms.CheckBox chkFilterLosses;
        private System.Windows.Forms.CheckBox chkFilterKills;
        private System.Windows.Forms.ComboBox cboMainKillType;
        private System.Windows.Forms.ComboBox cbo2ndType;
        private System.Windows.Forms.TextBox txtSearchBox;
        private System.Windows.Forms.Button bttnSearch;
        private System.Windows.Forms.Button bttnClearHUD;
        private System.Windows.Forms.TableLayoutPanel tlpKillDisplay;
        private System.Windows.Forms.ListView listView1;
    }
}