﻿namespace eveHunter
{
    partial class dlgAPI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bttnCheckAPI = new System.Windows.Forms.Button();
            this.bttnClearAPI = new System.Windows.Forms.Button();
            this.bttnSaveAPI = new System.Windows.Forms.Button();
            this.bttnCancel = new System.Windows.Forms.Button();
            this.cboCharaters = new System.Windows.Forms.ComboBox();
            this.txtAPIName = new System.Windows.Forms.TextBox();
            this.txtAPIKeyID = new System.Windows.Forms.TextBox();
            this.txtAPIvCode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblAPIStatus = new System.Windows.Forms.Label();
            this.lblExp = new System.Windows.Forms.Label();
            this.chkDefaultAPI = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // bttnCheckAPI
            // 
            this.bttnCheckAPI.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.bttnCheckAPI.Location = new System.Drawing.Point(24, 131);
            this.bttnCheckAPI.Name = "bttnCheckAPI";
            this.bttnCheckAPI.Size = new System.Drawing.Size(75, 23);
            this.bttnCheckAPI.TabIndex = 0;
            this.bttnCheckAPI.Text = "Verify API";
            this.bttnCheckAPI.UseVisualStyleBackColor = true;
            this.bttnCheckAPI.Click += new System.EventHandler(this.bttnCheckAPI_Click);
            // 
            // bttnClearAPI
            // 
            this.bttnClearAPI.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.bttnClearAPI.Location = new System.Drawing.Point(154, 131);
            this.bttnClearAPI.Name = "bttnClearAPI";
            this.bttnClearAPI.Size = new System.Drawing.Size(75, 23);
            this.bttnClearAPI.TabIndex = 1;
            this.bttnClearAPI.Text = "Clear";
            this.bttnClearAPI.UseVisualStyleBackColor = true;
            this.bttnClearAPI.Click += new System.EventHandler(this.bttnClearAPI_Click);
            // 
            // bttnSaveAPI
            // 
            this.bttnSaveAPI.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.bttnSaveAPI.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bttnSaveAPI.Location = new System.Drawing.Point(284, 131);
            this.bttnSaveAPI.Name = "bttnSaveAPI";
            this.bttnSaveAPI.Size = new System.Drawing.Size(75, 23);
            this.bttnSaveAPI.TabIndex = 2;
            this.bttnSaveAPI.Text = "Save";
            this.bttnSaveAPI.UseVisualStyleBackColor = true;
            this.bttnSaveAPI.Click += new System.EventHandler(this.bttnSaveAPI_Click);
            // 
            // bttnCancel
            // 
            this.bttnCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.bttnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bttnCancel.Location = new System.Drawing.Point(414, 131);
            this.bttnCancel.Name = "bttnCancel";
            this.bttnCancel.Size = new System.Drawing.Size(75, 23);
            this.bttnCancel.TabIndex = 3;
            this.bttnCancel.Text = "Cancel";
            this.bttnCancel.UseVisualStyleBackColor = true;
            this.bttnCancel.Click += new System.EventHandler(this.bttnCancel_Click);
            // 
            // cboCharaters
            // 
            this.cboCharaters.FormattingEnabled = true;
            this.cboCharaters.Location = new System.Drawing.Point(120, 90);
            this.cboCharaters.Name = "cboCharaters";
            this.cboCharaters.Size = new System.Drawing.Size(239, 21);
            this.cboCharaters.TabIndex = 4;
            // 
            // txtAPIName
            // 
            this.txtAPIName.Location = new System.Drawing.Point(86, 9);
            this.txtAPIName.Name = "txtAPIName";
            this.txtAPIName.Size = new System.Drawing.Size(166, 20);
            this.txtAPIName.TabIndex = 5;
            // 
            // txtAPIKeyID
            // 
            this.txtAPIKeyID.Location = new System.Drawing.Point(86, 35);
            this.txtAPIKeyID.Name = "txtAPIKeyID";
            this.txtAPIKeyID.Size = new System.Drawing.Size(100, 20);
            this.txtAPIKeyID.TabIndex = 6;
            // 
            // txtAPIvCode
            // 
            this.txtAPIvCode.Location = new System.Drawing.Point(86, 64);
            this.txtAPIvCode.Name = "txtAPIvCode";
            this.txtAPIvCode.Size = new System.Drawing.Size(403, 20);
            this.txtAPIvCode.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "API Name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(35, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Key ID:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(36, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "vCode:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Default Character:";
            // 
            // lblAPIStatus
            // 
            this.lblAPIStatus.AutoSize = true;
            this.lblAPIStatus.Location = new System.Drawing.Point(281, 16);
            this.lblAPIStatus.Name = "lblAPIStatus";
            this.lblAPIStatus.Size = new System.Drawing.Size(16, 13);
            this.lblAPIStatus.TabIndex = 12;
            this.lblAPIStatus.Text = "...";
            // 
            // lblExp
            // 
            this.lblExp.AutoSize = true;
            this.lblExp.Location = new System.Drawing.Point(281, 38);
            this.lblExp.Name = "lblExp";
            this.lblExp.Size = new System.Drawing.Size(16, 13);
            this.lblExp.TabIndex = 13;
            this.lblExp.Text = "...";
            // 
            // chkDefaultAPI
            // 
            this.chkDefaultAPI.AutoSize = true;
            this.chkDefaultAPI.Location = new System.Drawing.Point(409, 92);
            this.chkDefaultAPI.Name = "chkDefaultAPI";
            this.chkDefaultAPI.Size = new System.Drawing.Size(80, 17);
            this.chkDefaultAPI.TabIndex = 14;
            this.chkDefaultAPI.Text = "Default API";
            this.chkDefaultAPI.UseVisualStyleBackColor = true;
            // 
            // dlgAPI
            // 
            this.AcceptButton = this.bttnCheckAPI;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.bttnCancel;
            this.ClientSize = new System.Drawing.Size(521, 166);
            this.ControlBox = false;
            this.Controls.Add(this.chkDefaultAPI);
            this.Controls.Add(this.lblExp);
            this.Controls.Add(this.lblAPIStatus);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtAPIvCode);
            this.Controls.Add(this.txtAPIKeyID);
            this.Controls.Add(this.txtAPIName);
            this.Controls.Add(this.cboCharaters);
            this.Controls.Add(this.bttnCancel);
            this.Controls.Add(this.bttnSaveAPI);
            this.Controls.Add(this.bttnClearAPI);
            this.Controls.Add(this.bttnCheckAPI);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "dlgAPI";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "API Information";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.dlgAPI_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bttnCheckAPI;
        private System.Windows.Forms.Button bttnClearAPI;
        private System.Windows.Forms.Button bttnSaveAPI;
        private System.Windows.Forms.Button bttnCancel;
        private System.Windows.Forms.ComboBox cboCharaters;
        private System.Windows.Forms.TextBox txtAPIName;
        private System.Windows.Forms.TextBox txtAPIKeyID;
        private System.Windows.Forms.TextBox txtAPIvCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblAPIStatus;
        private System.Windows.Forms.Label lblExp;
        private System.Windows.Forms.CheckBox chkDefaultAPI;
    }
}